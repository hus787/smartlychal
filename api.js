var Req = require('request');
var Q = require('q');
var fs = require('fs');

Q.longStackSupport = true;


var token = "",
    endpoint = "",
    persec = 600,
    limit = 600,
    reqs = [],
    tot = 0;

function time(){
  // returns time in seconds
  var date = new Date();
  return date.getTime()/1000;
}

function throttle(){

  // has side-effect use with care
  var len = reqs.length,
      t = time();

  if (len == 0){
    reqs.push(t);
    return false;
  }
  
  if (len<limit){
    if (t - reqs[0] <= persec){
      reqs.push(t);
      return false;
    } else {
      reqs.splice(0,1);
      return throttle();
    }
  } else if (t - reqs[0] <= persec){
    return true;
  } else {
    reqs.splice(0,1);
    return throttle();
  }
}



function log(err){
  // taken from SO

  var path = './request_log.txt',
      buffer = new Buffer(err + "\n");

  fs.open(path, 'a', function(err, fd) {
    if (err) {
      throw 'Error opening file: ' + err;
    } else {
      fs.write(fd, buffer, 0, buffer.length, null, function(err) {
        if (err) throw 'Error writing file: ' + err;
        fs.close(fd, function() {
        })
      });
    }
  });
}

function _requester(req){
  var thrt = throttle(),
      deferred = Q.defer();
  
  if (!thrt){
    req
    .on('response',
        function(response){
          deferred.resolve(response);
        })
    .on('error', function(err){
      log(err);
      console.log(err);
      deferred.reject(new Error(err));
    });
  } else {
    err = "Only " + limit + " request per "+ persec + " seconds can be made.";
    log(err);
    deferred.reject(new Error("req: " + err));
  }
  return deferred.promise;
}

function conPath(path){
  endpoint = module.exports.endpoint,
  token = module.exports.token;
  return endpoint + path + "?access_token=" + token;
}

function _get(path){
  return _requester(Req.get(conPath(path)))
}

function _post(path, data){
  return _requester(Req.post(conPath(path), data))

}


function _put(path, data){
  return _requester(Req.put(conPath(path), data))

}


function _del(path){
  return _requester(Req.del(conPath(path)))

}

module.exports = {
  put:_post,
  delete: _del,
  token: token,
  post: _post,
  get: _get,
  endpoint: endpoint,
  log: log
};