var mysql = require('mysql'),
    http = require('http'),
    cluster = require('cluster'),
    url = require('url'),
    express = require('express'),
    app = express(),
    numCPUs = require('os').cpus().length,
    PORT = 8080;

function getAdIds(requrl){
  var ads = url.parse(requrl, true).query.ad_ids;
  if (ads){
    return ads.split(/\D+/);
  } else {
    return [];
  }
}


function getQueryParam(requrl, param){
  return url.parse(requrl, true).query[param];
}

var pool = mysql.createPool({
  host     : 'localhost',
  user     : 'root',
  password : 'root',
  database: 'smartly'});

if (cluster.isMaster) {
  // http://stackoverflow.com/a/22207213/
  for (var i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on('exit', function(worker, code, signal) {
    console.log('worker ' + worker.process.pid + ' died');
  });
} else {

  app.get('/api/stats', function(req, res){
    
    res.set('Content-Type', 'application/json');
    var ads = getAdIds(req.url),
        startDate = getQueryParam(req.url, 'start_time'),
        endDate = getQueryParam(req.url, 'end_time'),
        dailycost = {}
        result = {};
    
    
    // do pooling
    pool.getConnection(function(err, connection) {

      // construct the query
      var query = "SELECT * FROM ad_statistics WHERE ad_id IN ("
      for (i=0; i<ads.length; i++){
        query = mysql.format(query + "?,", [ads[i]]);
      }
      query = mysql.format(query.slice(0,-1) // remove the last comma
                          + ") AND date>=? AND date<=?;", [startDate, endDate]);

      // make the query
      var queryStats = connection.query(query);
      queryStats
      .on('error', function(err) {
        // Handle error, an 'end' event will be emitted after this as well
        console.log(err);
      })
      .on('result', function(row) {
        // Pausing the connnection is useful if your processing involves I/O
        connection.pause();
        
        var ad = {'impressions': 0,
                  'clicks': 0,
                  'spent':0,
                  'ctr': 0,
                  'cpc': 0,
                  'cpm': 0,
                  'actions': {}};
        if (result[row.ad_id]){
          ad = result[row.ad_id];
        }
        
        ad.impressions += row.impressions;
        ad.clicks += row.clicks;
        ad.spent += row.spent;
        ad.ctr += (row.clicks* 100.0)/row.impressions;
        ad.cpc += row.spent/row.clicks;
        ad.cpm += row.spent*1000/row.impressions;
        if (!dailycost[row.ad_id]){
          dailycost[row.ad_id] = {}
        }
        dailycost[row.ad_id][row.date] = row.spent;
        
        result[row.ad_id] = ad;
        connection.resume();
      })
      .on("end", function (){

        // construct the query
        var query = "SELECT * FROM ad_actions WHERE ad_id IN ("
        for (i=0; i<ads.length; i++){
          query = mysql.format(query + "?,", [ads[i]]);
        }
        query = mysql.format(query.slice(0,-1) // remove the last comma
                            + ") AND date>=? AND date<=?;", [startDate, endDate]);
        
        // make the query
        var queryAction = connection.query(query);        
        queryAction
        .on('error', function(err) {
          // Handle error, an 'end' event will be emitted after this as well
          console.log(err);
        })
        .on('result', function(row) {
          // Pausing the connnection is useful if your processing involves I/O
          connection.pause();
          var ad = result[row.ad_id],
          
              actions = {'count': 0,
                         'value': 0,
                         'cpa': 0};
          if (ad) {             //just in case if the ad_id is present actions and not in statistics
            
            if(ad.actions[row.action]){
              actions = ad.actions[row.action];
            }
            
            actions.count += row.count;
            actions.value += row.value;
            actions.cpa += dailycost[row.ad_id][row.date]/actions.count; // not sure, but seems right
            result[row.ad_id].actions[row.action] = actions;
          }
          connection.resume();        
        })
        .on("end", function(){
          res.send(JSON.stringify(result, null, 4));
          connection.release();
        });
      });
    });
  });
  app.listen(PORT);
}


console.log('Server running at http://127.0.0.1:'+PORT+'/'); 

